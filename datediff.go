package main

import (
	"fmt"
	"os"
)

// this will be used to convert ascii values into numbers
const _ASCII_MAGIC_NUMBER = 48

const _EMPTY = ""
const _NULL = -1

// this is a collection of date part constants
type _DateConstant int

const (
	_JANUARY _DateConstant = iota + 1
	_FEBRUARY
	_MARCH
	_APRIL
	_MAY
	_JUNE
	_JULY
	_AUGUST
	_SEPTEMBER
	_OCTOBER
	_NOVEMBER
	_DECEMBER
	_DAY
	_MONTH
	_YEAR
)

// a Date class
type _Date struct {
	Day   int
	Month int
	Year  int
}

func main() {
	argsLen := len(os.Args)

	if argsLen > 1 {
		args := os.Args
		input1 := args[1]

		if input1 == "test" {
			// different days test
			_dateDiffTest("02/06/1983", "22/06/1983")
			// different days and month test
			_dateDiffTest("04/07/1984", "25/12/1984")
			// different month and year test
			_dateDiffTest("03/01/1989", "03/08/1983")
			// min and max allowed years test
			_dateDiffTest("01/01/1901", "01/01/2999")
			// all date parts are different test
			_dateDiffTest("04/07/1984", "25/12/1986")
			// max allowed year exceeded test
			_dateDiffTest("01/01/1901", "01/01/3000")
			// invalid format test
			_dateDiffTest("01/01/AAAA", "01/01/90")
			return
		} else if argsLen > 3 {
			input2 := args[3]
			var totalEllapsedDays int

			if argsLen > 4 && args[4] == "reverse" {
				fmt.Println("reversing inputs")
				totalEllapsedDays = dateDiff(input2, input1)
			} else {
				totalEllapsedDays = dateDiff(input1, input2)
			}

			if totalEllapsedDays != _NULL {
				fmt.Printf("%d days", totalEllapsedDays)
			} else {
				fmt.Print("Invalid date")
			}

			return
		}
	}

	fmt.Println("Usage: ./datediff DD/MM/YYYY - DD/MM/YYYY")
}

// process the 2 dates string inputs and prints out the fully elapsed days between those 2 dates.
func dateDiff(date1 string, date2 string) int {
	var maxDate, minDate = _convertToDate(date2), _convertToDate(date1)

	// validation check
	if !_isDateValid(maxDate) || !_isDateValid(minDate) {
		return _NULL
	}
	
	// assign the input dates to min and max dates
	// to ensure that the minuend gets the max date and the subtrahend gets the min date
	if _dateCompare(minDate, maxDate) > 0 {
		tempDate := maxDate
		maxDate = minDate
		minDate = tempDate
	}

	// set the initial value of the total ellapsed days to 0
	totalEllapsedDays := 0
	
	// if the min and max dates belongs to the same year
	if maxDate.Year == minDate.Year {
		if maxDate.Month != minDate.Month {
			// sum up the total days of each month between the min and max dates
			for month := minDate.Month + 1; month < maxDate.Month; month++ {
				totalEllapsedDays += _getDaysInAMonth(month, minDate.Year)
			}
			// then compute the remaining days in the min date
			// the max date day will just be added to the remaining days
			totalEllapsedDays += _calculateRemainingMinDays(minDate, maxDate.Day)
		} else {
			// compute the absolute day difference of the min and max dates
			dayDiff := _abs(maxDate.Day - minDate.Day)
			// because we only calculate fully ellapsed days, we only use the day difference if its more than 1 day
			if dayDiff > 1 {
				totalEllapsedDays += dayDiff
			}
		}
	} else {
		// sum up the number of days for each year in between the min and max dates
		for year := minDate.Year + 1; year < maxDate.Year; year++ {
			totalEllapsedDays += _getDaysInAYear(year)
		}

		// if the min and max dates are from different years
		// we calculate remaining days of each dates
		totalEllapsedDays += _calculateRemainingMinMaxDays(minDate, maxDate)
	}

	// deducts a day to the totalEllapsedDays if the totalEllapsedDays is more than 1 day
	// because the first and last days are considered partial days
	if totalEllapsedDays > 1 {
		totalEllapsedDays--
	}

	return totalEllapsedDays
}

// test helper
func _dateDiffTest(date1 string, date2 string) {
	fmt.Printf("%s - %s = %d days\n", date1, date2, dateDiff(date1, date2))
}

// a helper function to compare 2 string date to determine which of the 2 dates is bigger
// returns: 1 if the first date is bigger
//			-1 if the second date is bigger
//			0 if both dates are equal
func _dateCompare(date1 _Date, date2 _Date) int {
	if date1.Year != date2.Year {
		if date1.Year > date2.Year {
			return 1
		}
		return -1
	}

	if date1.Month != date2.Month {
		if date1.Month > date2.Month {
			return 1
		}
		return -1
	}

	if date1.Day != date2.Day {
		if date1.Day > date2.Day {
			return 1
		}
		return -1
	}

	return 0
}

// calculate the remaining days in a month, where year is used to calculate the leap year
// offset is the max date day
func _calculateRemainingMinDays(date _Date, offset int) int {
	return _getDaysInAMonth(date.Month, date.Year) - date.Day + offset
}

// helper function to calculate the remaining days of each dates
func _calculateRemainingMinMaxDays(minDate _Date, maxDate _Date) int {
	// calculate the remaining days of the min date
	// the max date day serves as the remaining days of the max date
	// so we also add that to the initial remaining days
	remainingDays := _calculateRemainingMinDays(minDate, maxDate.Day)

	// compute the remaining days of the max year starting from january up to the max month
	for month := 1; month < maxDate.Month; month++ {
		remainingDays += _getDaysInAMonth(month, maxDate.Year)
	}

	// compute for the rest of the min year starting from the min month + 1
	for month := minDate.Month + 1; month <= 12; month++ {
		remainingDays += _getDaysInAMonth(month, minDate.Year)
	}

	return remainingDays
}

// helper method to validate a date input
func _isDateValid(date _Date) bool {
	// if the computed year value is less than 1901 or greater than 2999 return 0
	return !(date.Year < 1901 ||  date.Year > 2999 || date.Month < 1 ||  date.Month > 12) && date.Day <= _getDaysInAMonth(date.Month, date.Year) && date.Day > 0
}

// helper method to convert a date string to _Date struct
func _convertToDate(dateString string) _Date {
	return _Date{
		Day:   _getDatePart(dateString, _DAY),
		Month: _getDatePart(dateString, _MONTH),
		Year:  _getDatePart(dateString, _YEAR),
	}
}

// helper method that will extract a specific date part from the input date string
// returns the integer value version of the date part
func _getDatePart(dateString string, datePart _DateConstant) int {
	datePartValue := _extractDatePartValue(dateString, datePart)

	if datePartValue == _EMPTY {
		return _NULL
	}

	// year date part section
	if datePart == _YEAR {
		return ((int(datePartValue[0]) - _ASCII_MAGIC_NUMBER) * 1000) + ((int(datePartValue[1]) - _ASCII_MAGIC_NUMBER) * 100) + ((int(datePartValue[2]) - _ASCII_MAGIC_NUMBER) * 10) + (int(datePartValue[3]) - _ASCII_MAGIC_NUMBER)
	}

	return ((int(datePartValue[0]) - _ASCII_MAGIC_NUMBER) * 10) + int(datePartValue[1]) - _ASCII_MAGIC_NUMBER
}

// helper function to convert the given input to its absolute value
func _abs(value int) int {
	if value < 0 {
		return -value
	}

	return value
}

// helper function to check if the given year is a leap year
func _isLeapYear(year int) bool {
	return (year%4 == 0 && year%100 != 0) || year%400 == 0
}

// helper function to get how many days are there in a year including leap years
func _getDaysInAYear(year int) int {
	if _isLeapYear(year) {
		return 366
	}
	return 365
}

// helper function to get how many days are there in a specific month for a specific year
func _getDaysInAMonth(month int, year int) int {
	monthConstant := _DateConstant(month)
	if monthConstant == _JANUARY || monthConstant == _MARCH || monthConstant == _MAY || monthConstant == _JULY || monthConstant == _AUGUST || monthConstant == _OCTOBER || monthConstant == _DECEMBER {
		return 31
	}

	if monthConstant == _APRIL || monthConstant == _JUNE || monthConstant == _SEPTEMBER || monthConstant == _NOVEMBER {
		return 30
	}

	if monthConstant == _FEBRUARY {
		if _isLeapYear(year) {
			return 29
		}
		return 28
	}

	return _NULL
}

// helper function to extract a specific date part of a date string
func _extractDatePartValue(dateString string, datePart _DateConstant) string {
	// validation check
	if len(dateString) != 10 {
		return _EMPTY
	}

	if datePart == _YEAR {
		return dateString[6:10]
	}

	if datePart == _MONTH {
		return dateString[3:5]
	}

	if datePart == _DAY {
		return dateString[:2]
	}

	return _EMPTY
}
