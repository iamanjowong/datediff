# datediff

A simple date calculator that will calculate the fully ellapsed days between 2 dates

# requirements

- [Golang](https://golang.org/doc/install)

# building

- go build


# testing

- datediff test

# running

- datediff DD/MM/YYYY - DD/MM/YYYY
